import { render, screen, cleanup } from "@testing-library/react";
// Importing the jest testing library
import '@testing-library/jest-dom'
import Counter from "./counter";

afterEach(() => {
    cleanup(); 
})
  
describe("Counter Component" ,() => {
    const book = {
        "id": 21,
        "author": "Garann Means",
        "price": 41.89,
        "image": "https://courses.prometheus.org.ua/asset-v1:Ciklum+FEB101+2022_T3+type@asset+block@node_for_front_end_developers.jpg",
        "title": "Node for Front-End Developers",
        "shortDescription": "If you know how to use JavaScript in the browser, you already have the skills you need to put JavaScript to work on back-end servers with Node.",
        "description": "If you know how to use JavaScript in the browser, you already have the skills you need to put JavaScript to work on back-end servers with Node. This hands-on book shows you how to use this popular JavaScript platform to create simple server applications, communicate with the client, build dynamic pages, work with data, and tackle other tasks. Although Node has a complete library of developer-contributed modules to automate server-side development, this book will show you how to program with Node on your own, so you truly understand the platform. Discover firsthand how well Node works as a web server, and how easy it is to learn and use."
    };
    render(<Counter book={book}/>); 
    const buttonUp = screen.getByTestId("up"); 
      
    // Test 1
    test("Button Rendering", () => {
        expect(buttonUp).toBeInTheDocument(); 
    })
  
    // Test 2 
    // test("Button Text", () => {
    //     expect(button).toHaveTextContent("Click Me!"); 
    // })
})